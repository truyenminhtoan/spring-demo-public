FROM openjdk:8-jdk-alpine

ENV PROFILE_ACTIVE dev

WORKDIR /app

# Add Maintainer Info
LABEL maintainer="info@bstarsolutions.com"

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=build/libs/*.jar

# Add the application's jar to the container
ADD ${JAR_FILE} /app/app.jar

# Run the jar file
ENTRYPOINT [ "sh", "-c", "java -Dspring.profiles.active=${PROFILE_ACTIVE} -Djava.security.egd=file:/dev/./urandom -jar /app/app.jar" ]